// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyBIYe53InvftsKcb3K54c5GWCW_gZDb17I',
    authDomain: 'redstone-4f535.firebaseapp.com',
    projectId: 'redstone-4f535',
    storageBucket: 'redstone-4f535.appspot.com',
    messagingSenderId: '313383107270',
    appId: '1:313383107270:web:2c3c51b7de1c81360835c7',
    measurementId: 'G-TBN30PKPDS',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
