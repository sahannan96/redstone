import { Component, OnInit, ViewChild } from '@angular/core';
import { EventDialogComponent } from './event-dialog/event-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import * as moment from 'moment';
import * as _ from 'lodash';
import { AppService } from './app.service';
import { take } from 'rxjs/operators';
import { CalendarEvent } from './interface/Event';
import { ConfirmDeleteComponent } from './confirm-delete/confirm-delete.component';
export interface CalendarDate {
  mDate: moment.Moment;
  selected?: boolean;
  today?: boolean;
  events?: CalendarEvent[];
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  public currentDate: moment.Moment = moment();
  public namesOfDays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  public weeks: Array<CalendarDate[]> = [];

  public selectedDate: any;
  todayEvents: CalendarEvent[] = [];
  allEvents: CalendarEvent[] = [];

  @ViewChild('calendar', { static: true }) calendar: any;

  constructor(public dialog: MatDialog, private appService: AppService) {}

  ngOnInit() {
    this.currentDate = moment();
    this.selectedDate = moment(this.currentDate).format('DD/MM/YYYY');
    this.getAllEvents();
  }

  private generateCalendar() {
    const dates = this.fillDates(this.currentDate);
    const weeks = [];
    while (dates.length > 0) {
      weeks.push(dates.splice(0, 7));
    }
    this.weeks = weeks;
  }

  private fillDates(currentMoment: moment.Moment) {
    const firstOfMonth = moment(currentMoment).startOf('month').day();
    const lastOfMonth = moment(currentMoment).endOf('month').day();

    const firstDayOfGrid = moment(currentMoment)
      .startOf('month')
      .subtract(firstOfMonth, 'days');
    const lastDayOfGrid = moment(currentMoment)
      .endOf('month')
      .subtract(lastOfMonth, 'days')
      .add(7, 'days');

    const startCalendar = firstDayOfGrid.date();

    return _.range(
      startCalendar,
      startCalendar + lastDayOfGrid.diff(firstDayOfGrid, 'days')
    ).map((date) => {
      const newDate = moment(firstDayOfGrid).date(date);

      const events = this.setAllEvents(newDate);

      return {
        today: this.isToday(newDate),
        selected: this.isSelected(newDate),
        mDate: newDate,
        events: events,
      };
    });
  }

  public prevMonth(): void {
    this.currentDate = moment(this.currentDate).subtract(1, 'months');
    this.generateCalendar();
  }

  public nextMonth(): void {
    this.currentDate = moment(this.currentDate).add(1, 'months');
    this.generateCalendar();
  }

  public isDisabledMonth(currentDate: any): boolean {
    const today = moment();
    return moment(currentDate).isBefore(today, 'months');
  }

  private isToday(date: moment.Moment): boolean {
    return moment().isSame(moment(date), 'day');
  }

  private isSelected(date: moment.Moment): boolean {
    return this.selectedDate === moment(date).format('DD/MM/YYYY');
  }

  public isSelectedMonth(date: moment.Moment): boolean {
    const today = moment();
    return (
      moment(date).isSame(this.currentDate, 'month') &&
      moment(date).isSameOrBefore(today)
    );
  }

  public selectDate(date: CalendarDate) {
    this.selectedDate = moment(date.mDate).format('DD/MM/YYYY');
    const todayEvents = [];
    date?.events?.forEach((element) => {
      todayEvents.push(element);
    });
    this.todayEvents = todayEvents;
    console.log(todayEvents);
    this.generateCalendar();
  }

  addEvent() {
    const dialogRef = this.dialog.open(EventDialogComponent, {
      data: this.selectedDate,
    });

    dialogRef.afterClosed().subscribe(() => {
      this.allEvents=[]
      this.todayEvents=[]
      this.getAllEvents();
    });
  }

  getAllEvents() {
    const allEvents = [];
    this.appService
      .getEvents()
      .pipe(take(1))
      .subscribe({
        next: (events) => {
          events.forEach((event, index) => {
            event.events.forEach((element) => {
              allEvents.push(element);
            });
          });
          this.allEvents = allEvents;
          console.log(this.allEvents);
          this.generateCalendar();
        },
      });
  }

  setAllEvents(date: any) {
    const todayEvents: CalendarEvent[] = [];

    this.allEvents?.forEach((event) => {
      if (
        event.startDate.toDate().toDateString() === date.toDate().toDateString()
      ) {
        todayEvents.push(event);
      }
    });
    return todayEvents;
  }

  editEvent(data: any) {
    console.log(data);
  }

  deleteEvent(event: any) {
    const dialogRef = this.dialog.open(ConfirmDeleteComponent, {
      data: event,
    });

    dialogRef.afterClosed().subscribe(() => {
      this.todayEvents=[]
      console.log('reload initiated');
      this.getAllEvents();
    });
  }
}
