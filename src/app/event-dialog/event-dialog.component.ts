import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as moment from 'moment';
import { Colors } from '../../assets/colors';
import { AppService } from '../app.service';
import { CalendarEvent } from '../interface/Event';

@Component({
  selector: 'app-event-dialog',
  templateUrl: './event-dialog.component.html',
  styleUrls: ['./event-dialog.component.scss'],
})
export class EventDialogComponent implements OnInit {
  eventForm: FormGroup;
  colors = [];
  defaultColor: any = {};
  arrayObject: { events: CalendarEvent[] };

  constructor(
    @Inject(MAT_DIALOG_DATA) public date: string,
    private appService: AppService
  ) {
    this.colors = Object.keys(Colors).map((key) => ({
      name: key,
      value: Colors[key],
    }));
    this.defaultColor = {
      name: this.colors[0],
      value: this.colors[0],
    };
  }

  ngOnInit() {
    this.createFormGroup();
    this.eventForm.get('color').setValue(this.defaultColor.name);
  }

  createFormGroup() {
    this.eventForm = new FormGroup({
      eventTitle: new FormControl(''),
      eventDescription: new FormControl(''),
      startDate: new FormControl(
        new Date(moment(this.date, 'DD/MM/YYYY').format('MM DD YYYY'))
      ),
      endDate: new FormControl(
        new Date(moment(this.date, 'DD/MM/YYYY').format('MM DD YYYY'))
      ),
      color: new FormControl(''),
    });
  }

  createEvent() {
    this.createObject();
    this.appService.createEvent(this.arrayObject);
  }

  createObject() {
    const dateDifference = this.getDateRange(
      this.eventForm.get('startDate').value,
      this.eventForm.get('endDate').value
    );

    if (dateDifference.length === 1) {
      this.createSingleEvent(dateDifference);
    } else {
      this.createMultipleEvents(dateDifference);
    }
  }

  getDateRange(startDate, endDate) {
    const dateArray = [];
    let currentDate = new Date(startDate);

    while (currentDate <= new Date(endDate)) {
      dateArray.push(new Date(currentDate));
      currentDate.setUTCDate(currentDate.getUTCDate() + 1);
    }

    return dateArray;
  }

  createSingleEvent(dates: Date[]) {
    const singleEventArray: CalendarEvent[] = [];
    dates.forEach((date) => {
      const singleEvent: CalendarEvent = {
        startDate: date,
        endDate: this.eventForm.get('endDate').value,
        color: this.eventForm.get('color').value,
        title: this.eventForm.get('eventTitle').value,
        description: this.eventForm.get('eventDescription').value,
        single: true,
      };
      singleEventArray.push(singleEvent);
    });
    this.arrayObject = { events: singleEventArray };
  }

  createMultipleEvents(dates: Date[]) {
    const multipleEventsArray: CalendarEvent[] = [];
    dates.forEach((date) => {
      const multipleEvents: CalendarEvent = {
        startDate: date,
        endDate: this.eventForm.get('endDate').value,
        color: this.eventForm.get('color').value,
        title: this.eventForm.get('eventTitle').value,
        description: this.eventForm.get('eventDescription').value,
        single: false,
      };
      multipleEventsArray.push(multipleEvents);
    });
    this.arrayObject = { events: multipleEventsArray };
  }
}
