import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppService } from '../app.service';
import { CalendarEvent } from '../interface/Event';

@Component({
  selector: 'app-confirm-delete',
  templateUrl: './confirm-delete.component.html',
  styleUrls: ['./confirm-delete.component.scss'],
})
export class ConfirmDeleteComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: CalendarEvent,
    private appService: AppService
  ) {}

  deleteEvent() {
    this.appService.deleteEvent(this.data.id);
  }
}
