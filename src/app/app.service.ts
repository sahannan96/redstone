import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreDocument,
} from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AppService {
  constructor(public afs: AngularFirestore) {}

  getEvents(): Observable<any[]> {
    return this.afs.collection(`events`).valueChanges();
  }

  createEvent(eventObject: any) {
    const id = this.afs.createId();
    let newEventObject = this.setID(eventObject, id);
    const packageRef: AngularFirestoreDocument<any> = this.afs.doc(
      `events/${id}`
    );
    return packageRef.set(newEventObject, { merge: true });
  }

  deleteEvent(id: string) {
    return this.afs.collection('events').doc(id).delete();
  }

  setID(eventObject: any, id: string) {
    eventObject.events.forEach((element) => {
      element.id = id;
    });
    return eventObject;
  }
}
