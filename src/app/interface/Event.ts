export interface CalendarEvent {
  id?: string;
  startDate: any;
  endDate: any;
  color?: string;
  title: string;
  description?: string;
  single: boolean;
}